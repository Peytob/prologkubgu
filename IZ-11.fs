﻿open System
open System.Windows.Forms
open System.Drawing

// Установка главной формы

let FORM = new Form(Width = 400, Height = 300, Text = "Some rectangle area") 

// Прямоугольничек (ляпота!)

let formPain(e : PaintEventArgs) =
    let rectagnlePen = new Pen(Color.Black, 5.0f)
    let rectangleX = 250
    let rectangleY = 20
    let rectangleH = 140
    let rectangleW = 100

    let a = new Point(rectangleX, rectangleY)
    let b = new Point(rectangleX + rectangleW, rectangleY)
    let c = new Point(rectangleX, rectangleY + rectangleH)
    let d = new Point(rectangleX + rectangleW, rectangleY + rectangleH)

    e.Graphics.DrawLine(rectagnlePen, a, b)
    e.Graphics.DrawLine(rectagnlePen, a, c)
    e.Graphics.DrawLine(rectagnlePen, c, d)
    e.Graphics.DrawLine(rectagnlePen, d, b)
FORM.Paint.Add formPain

// Буквы - метки на его сторонах

let LABEL_A_MARK = new Label(Text = "A", Left = 225, Top = 80, Height = 20, Width = 20)
FORM.Controls.Add LABEL_A_MARK

let LABEL_B_MARK = new Label(Text = "B", Left = 295, Top = 170, Height = 20, Width = 20)
FORM.Controls.Add LABEL_B_MARK

// Поля для ввода данных

let TEXTBOX_A = new TextBox(Text = "A sizes", Left = 20, Top = 20, Height = 30, Width = 120)
FORM.Controls.Add TEXTBOX_A

let TEXTBOX_B = new TextBox(Text = "B sizes", Left = 20, Top = 70, Height = 30, Width = 120)
FORM.Controls.Add TEXTBOX_B

// Кнопочка для высчитывания площади

let ACCEPT_BUTTON = new Button(Dock = DockStyle.Bottom, Height = 50, Text = "Compute area")
FORM.Controls.Add ACCEPT_BUTTON

let acceptButtonClick (e : EventArgs) =
    try
        let aSide = int(TEXTBOX_A.Text)
        let bSide = int(TEXTBOX_B.Text)
        MessageBox.Show("Area is " + (aSide * bSide).ToString())
    with
        | :? Exception -> MessageBox.Show "Incorrect input!"
    |> ignore

ACCEPT_BUTTON.Click.Add acceptButtonClick

do Application.Run(FORM)

[<EntryPoint>]
let main argv =
    0 // return an integer exit code