% Проверка числа. Принимает числа от 1 до 10^10 - 1 (у верхнего потолка всегда false)
isPandigital(Number, N) :-
	WaitSumm is N * (N + 1) / 2, % Тут вместо N использую N+1, так как мы не учитываем нолик.
	isPandigital_(Number, 0, WaitSumm), !.

isPandigital_(0, Summ, WaitSumm) :-
	Summ = WaitSumm.

isPandigital_(CurrentNumber, Summ, WaitSumm) :-
	not(CurrentNumber = 0),
	CurrentNumberUpd is CurrentNumber div 10,
	CurrentDigit is CurrentNumber mod 10,

	SummUpd is Summ + CurrentDigit,

	isPandigital_(CurrentNumberUpd, SummUpd, WaitSumm).

% Проверка числа на простоту.
isPrime(Number) :-
	NumberNotChetnoe is Number mod 2,
	NumberNotChetnoe = 1,

	NumberSqrt is sqrt(Number) + 1, % Оптимизирую. Надеюсь, можно использовать sqrt
	NumberSqrtSuperInt is round(float_integer_part(NumberSqrt)) + 1,
	isPrime_(Number, NumberSqrtSuperInt, 3), !.

isPrime_(_, NumberSqrt, CurrentNumber) :-
	CurrentNumber >= NumberSqrt.

isPrime_(Number, NumberSqrt, CurrentNumber) :-
	CurrentNumber < NumberSqrt,
	SomeTempValue is Number mod CurrentNumber,
	not(SomeTempValue = 0),
	
	NextNumber is CurrentNumber + 2,
	isPrime_(Number, NumberSqrt, NextNumber).

isPrime_(Number, NumberSqrt, CurrentNumber) :-
	CurrentNumber < NumberSqrt,
	SomeTempValue is Number mod CurrentNumber,
	SomeTempValue = 0, fail, !.

% Нахождение наибольшего требуемого числа от 10^N до 10^(N+1)-1
iz1(N, Biggest) :-
	From is 10**(N-1),
	To is 10**N,
	iz1Work(From, To, -1, N, Biggest), !.

iz1Work(Current, To, _, NMem, Biggest) :-
	Current < To,
	(isPrime(Current), isPandigital(Current, NMem)),
	CurrentNext is Current + 1,
	iz1Work(CurrentNext, To, Current, NMem, Biggest), !.

iz1Work(Current, To, BiggestCurrent, NMem, Biggest) :-
	Current < To,
	CurrentNext is Current + 1,
	iz1Work(CurrentNext, To, BiggestCurrent, NMem, Biggest), !.

iz1Work(Current, To, BiggestCurrent, _, Biggest) :-
	Current = To,
	write("NYA"),
	Biggest = BiggestCurrent, !.
