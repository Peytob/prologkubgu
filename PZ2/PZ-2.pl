max(X,Y,X) :-
    X > Y.
max(_,Y,Y).

max_3(X, Y, U, Z) :-
    max(X, Y, T),
    max(T, U, Z).

fibonachi(0, 0).
fibonachi(1, 1).
fibonachi(2, 1).
fibonachi(X, Result) :-
    X_1 is X - 1,
    X_2 is X - 2,
    fibonachi(X_1, Result_1),
    fibonachi(X_2, Result_2),
    Result is Result_1 + Result_2.

isSimple(X) :-
    isSimple(2, X).
isSimple(X, X) :- !.
isSimple(K, X) :-
    J is X mod K,
    J = 0,
    !, fail.
isSimple(K, X) :-
    K1 is K,
    isSimple(K1, X).

heigestSimpleDivider(X, N) :-
    heigestSimpleDivider(X, X, N).

heigestSimpleDivider(X, K, K) :-
    Ost is X mod K,
    Ost = 0,
    isSimple(K), !.
heigestSimpleDivider(X, K, N) :-
    K1 is K-1,
    heigestSimpleDivider(X, K1, N).

