import re

from tkinter import *
from tkinter import messagebox

from pyswip import Prolog
import pyswip

# = = = = = = = = = = = =
# Проверка и обработка ввода
# = = = = = = = = = = = =

def processEdgesInput(text: str) -> set:
	# Подготовочка и создание множества
	lines = text.splitlines()
	lines = set(lines)
	if '' in lines:
		lines.remove('')
	
	errorString = str() # Строка для возможного исключения

	# Проверка соответствия
	linesProcessed = set()
	for line in lines:
		if re.match(R"\s*\d+\s*-\s*\d+$", line):
			linesProcessed.add(line)
		else:
			string = re.sub("\s+", " ", line)
			errorString += f"Неверно введено ребро: \"{line}\"\n"

	if len(errorString) != 0:
		raise RuntimeError(f"Найдены ошибки ввода ребер:\n{errorString}")

	del lines

	# Обработка и исключение ребер вида а - б, б - а
	output = list()
	for line in linesProcessed:
		vertexes = re.findall(f"\d+", line)
		edge = (int(min(vertexes)), int(max(vertexes)))
		if edge not in output:
			output.append(edge)
	
	return output

def processVertexesInput(text: str) -> set:
	text = re.sub(R"\s{2,}", " ", text)
	
	# Проверка соответствия и парсинг

	errorString = str() # Строка для возможного исключения
	output = set()
	for vertex in text.split():
		if re.match(R"\d+$", vertex):
			output.add(int(vertex))
		else:
			errorString += f"Вершина \"{vertex}\" имеет неправильный формат!\n"

	if len(errorString) != 0:
		raise RuntimeError(f"Найдены ошибки ввода вершин:\n{errorString}")

	return output

def prologUsing(vertexesText: str, edgesText: str):
	try:
		vertexes = processVertexesInput(vertexesText)
		edges = processEdgesInput(edgesText)
	except RuntimeError as exp:
		messagebox.showerror("Ошибка!", exp)
		return

	# Объявление и загрузка
	prolog = Prolog()
	prolog.consult("IZ-6.pl")

	# Загрузка фактов в пролог
	for v1, v2 in edges:
		prolog.assertz(f"hasEdge({v1}, {v2})")
	
	for v in vertexes:
		prolog.assertz(f"vertex({v})")

	prolog.dynamic("vertex/1")
	prolog.dynamic("hasEdge/2")
	prolog.dynamic("pythonOutput/1")

	prologCode = """
		forall(
			(
				hasEdge(StartFirst, StartSecond),
				vertex(StartSecond)
			),
			(
				getCliquesAutomatic_([StartFirst, StartSecond])
			)
		)
	"""

	# Он умеет что-то интерпритировать только в циклах, больше никак не умеет. П - П и т о н
	for i in prolog.query(prologCode):
		pass

	showText = "Найдены клики!\n"
	for i in prolog.query("pythonOutput(X)"):
		strFormat = ''.join([f'{j}, ' for j in i['X']])
		strFormat = strFormat[0 : -2]

		showText += f"{strFormat}\n"

	messagebox.showinfo("Клики", showText)

	# Боже, почему ты без фора ничего не делаешь?!

	for i in prolog.query("abolish(pythonOutput/1)"):
		pass

	for i in prolog.query("abolish(vertex/1)"):
		pass

	for i in prolog.query("abolish(hasEdge/1)"):
		pass

# = = = = = = = = = = = =
# Установка окна
# = = = = = = = = = = = =

if __name__ == "__main__":
	# Настройки окна
	window = Tk()
	window.geometry("800x300")
	window.title("Maximal Cliques Prolog")
	# window.resizable(width = False, height = False)
	window.configure(background='gray')

	# Текстовое поле для ввода ребер
	edgesText = Text(wrap = WORD)
	edgesText.place(relx = 0.5, rely = 0.1, relwidth = 0.45, relheight= 0.70)

	helpText =  "Введите сюда ребра в следующем формате:\nОткуда - Куда.\nОдно ребро на строку. Пример:\n"
	helpText += "1 - 2\n"
	helpText += "1 - 3\n"
	helpText += "3 - 5\n"
	edgesText.insert(1.0, helpText)

	# Текстовое поле для ввода вершин
	vertexesText = Text(wrap = WORD)
	vertexesText.place(relx = 0.05, rely = 0.1, relwidth = 0.45, relheight = 0.70)

	helpText =  "Введите сюда вершины, разделяя их пробелом. Название вершины - целое, положительное число. Пример:\n"
	helpText += "1 2 3 4 5 ..."
	vertexesText.insert(1.0, helpText)

	# Кнопочка ^^
	acceptButton = Button(text = "Прологовая магия, ура!")
	acceptButton.place(relx = 0.05, rely = 0.85, relwidth = 0.90, relheight = 0.1)
	acceptButton["command"] = lambda: prologUsing(vertexesText.get('1.0', END + '-1c'), edgesText.get('1.0', END + '-1c'))

	window.mainloop()

# Быстрый ввод графа с примера в ИЗ-6:

# 1 - 2
# 1 - 3
# 1 - 4
# 2 - 3
# 2 - 4
# 3 - 4
# 3 - 5
# 4 - 5

# 1 2 3 4 5
