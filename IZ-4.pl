% Лекционные предикаты

inList([El|_],El).
inList([_|T],El):-inList(T,El).

% Сама ИЗ

/*
Ответ:
Имена:     Balash,      Bela,      Aladar
Профессия: Buhgalter,   Aptekar,   Agronom
Город:     Bekeshchaba, Budapesht, Asod
*/

getListHead([H|_], Result) :- Result = H.
getListHead([], Result) :- fail.

boy("Balash").
boy("Bela").
boy("Aladar").

prof("Buhgalter").
prof("Aptekar").
prof("Agronom").

town("Bekeshchaba").
town("Budapesht").
town("Asod").

boys :-
	/* Берем 3 разных мужчины, каждому профессию и город */

	boy(FirstName),
	prof(FirstProf),
	town(FirstTown),

	boy(SecondName),
	not(FirstName = SecondName),
	prof(SecondProf),
	not(FirstProf = SecondProf),
	town(SecondTown),
	not(FirstTown = SecondTown),

	boy(ThirdName),
	not(FirstName = ThirdName), not(SecondName = ThirdName),
	prof(ThirdProf),
	not(FirstProf = ThirdProf), not(SecondProf = ThirdProf),
	town(ThirdTown),
	not(FirstTown = ThirdTown), not(SecondTown = ThirdTown),

	% Имя - профессия - город. Сгруппируем данные для удобства.
	List = [[FirstName, FirstProf, FirstTown], [SecondName, SecondProf, SecondTown], [ThirdName, ThirdProf, ThirdTown]],

	% Балаш не находится в Будапеште
	(
		not(inList(List, ["Balash", _, "Budapesht"]))
	),

	% Первые буквы у 2 человек из 3
	% atom_codes - преобразует строку в список символов. "abc123" => [97, 98, 99, 49, 50, 51]
	(
		% У первого и второго
		(
			atom_codes(FirstName, [N1|_]),
			atom_codes(FirstProf, [P1|_]),
			atom_codes(FirstTown, [T1|_]),
			N1 = P1, N1 = T1, P1 = T1,

			atom_codes(SecondName, [N2|_]),
			atom_codes(SecondProf, [P2|_]),
			atom_codes(SecondTown, [T2|_]),
			N2 = P2, N2 = T2, P2 = T2
		);

		% У первого и третьего
		(
			atom_codes(FirstName, [N1|_]),
			atom_codes(FirstProf, [P1|_]),
			atom_codes(FirstTown, [T1|_]),
			N1 = P1, N1 = T1, P1 = T1,

			atom_codes(ThirdName, [N2|_]),
			atom_codes(ThirdProf, [P2|_]),
			atom_codes(ThirdTown, [T2|_]),
			N2 = P2, N2 = T2, P2 = T2
		);

		% У второго и третьего
		(
			atom_codes(SecondName, [N1|_]),
			atom_codes(SecondProf, [P1|_]),
			atom_codes(SecondTown, [T1|_]),
			N1 = P1, N1 = T1, P1 = T1,

			atom_codes(ThirdName, [N2|_]),
			atom_codes(ThirdProf, [P2|_]),
			atom_codes(ThirdTown, [T2|_]),
			N2 = P2, N2 = T2, P2 = T2
		)
	),

	% Аптекарь находится в Будапеште
	(
		inList(List, [_, "Aptekar", "Budapesht"])
	),

	write(List), nl, !.
