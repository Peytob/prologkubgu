/*
	����� � �������� �� ������������ ���� Minecraft. ������ ���� ��� ���������!
	�����������:
	1. ����� �� ����� ������ ����/������� � ����������� ��������� ����? ��������� �� ���������
	2. �������� �� ������ ����/������� ��������������?
	3. ����� �� ������ ����/������� ������������ ������? ���������, �������, ��������� � ��� �����.
	4. ����� �� ������ ����/������� ������� ��������� (���� �������)/������������������ (���� ����).
	5. ����� �� ����� ������� ������ ����/�������?
	6. ����������� �� ������ ������� � ���� ���������?
	��������: ����������� �������� ��������� � ������ ����������� ��� ���� �������!
	������ �������� �������� ������������ ������������� ����������� �� ��������������� ���������� ��� �������� ����� �� 12 ��� �
	��������� ������ ������������ ���� "Minecraft".
*/

/* ������
0 - ���
1 - ��
2 - ��������
3 - �� ����
*/

/* ��������� */
:- dynamic worldGeneration/2. % ����� �� ����� ������ ����/������� � ����������� ��������� ����? ��������� �� ���������
:- dynamic renewable/2. % �������� �� ������ ����/������� ��������������?
:- dynamic benefit/2. % ����� �� ������ ����/������� ������������ ������?
:- dynamic highStrength/2. % ����� �� ������ ����/������� ������� ��������� (���� �������)/������������������ (���� ����).
:- dynamic craftable/2. % ����� �� ����� ������� ������ ����/�������?
:- dynamic treasure/2. % ����������� �� ������ ������� � ���� ���������?

/* ���� */

    /* --- ����� --- */

:- assert(worldGeneration("�������� ������", 1)).
:- assert(renewable("�������� ������", 0)).
:- assert(benefit("�������� ������", 0)).
:- assert(highStrength("�������� ������", 1)).
:- assert(craftable("�������� ������", 0)).
:- assert(treasure("�������� ������", 0)).

:- assert(worldGeneration("������", 1)).
:- assert(renewable("������", 0)).
:- assert(benefit("������", 0)).
:- assert(highStrength("������", 0)).
:- assert(craftable("������", 0)).
:- assert(treasure("������", 0)).

:- assert(worldGeneration("�����", 1)).
:- assert(renewable("�����", 0)).
:- assert(benefit("�����", 0)).
:- assert(highStrength("�����", 0)).
:- assert(craftable("�����", 0)).
:- assert(treasure("�����", 0)).

:- assert(worldGeneration("����", 1)).
:- assert(renewable("����", 1)).
:- assert(benefit("����", 0)).
:- assert(highStrength("����", 0)).
:- assert(craftable("����", 0)).
:- assert(treasure("����", 0)).

:- assert(worldGeneration("����", 1)).
:- assert(renewable("����", 1)).
:- assert(benefit("����", 1)).
:- assert(highStrength("����", 1)).
:- assert(craftable("����", 0)).
:- assert(treasure("����", 0)).

:- assert(worldGeneration("��������� ����", 0)).
:- assert(renewable("��������� ����", 0)).
:- assert(benefit("��������� ����", 0)).
:- assert(highStrength("��������� ����", 1)).
:- assert(craftable("��������� ����", 1)).
:- assert(treasure("��������� ����", 0)).

:- assert(worldGeneration("��������", 1)).
:- assert(renewable("��������", 0)).
:- assert(benefit("��������", 0)).
:- assert(highStrength("��������", 0)).
:- assert(craftable("��������", 1)).
:- assert(treasure("��������", 0)).

:- assert(worldGeneration("������� ������", 1)).
:- assert(renewable("������� ������", 1)).
:- assert(benefit("������� ������", 1)).
:- assert(highStrength("������� ������", 0)).
:- assert(craftable("������� ������", 1)).
:- assert(treasure("������� ������", 0)).

:- assert(worldGeneration("���", 1)).
:- assert(renewable("���", 1)).
:- assert(benefit("���", 0)).
:- assert(highStrength("���", 0)).
:- assert(craftable("���", 1)).
:- assert(treasure("���", 0)).

:- assert(worldGeneration("���� ���������", 0)).
:- assert(renewable("���� ���������", 1)).
:- assert(benefit("���� ���������", 1)).
:- assert(highStrength("���� ���������", 0)).
:- assert(craftable("���� ���������", 0)).
:- assert(treasure("���� ���������", 0)).

:- assert(worldGeneration("���������� �����", 1)).
:- assert(renewable("���������� �����", 1)).
:- assert(benefit("���������� �����", 0)).
:- assert(highStrength("���������� �����", 0)).
:- assert(craftable("���������� �����", 1)).
:- assert(treasure("���������� �����", 1)).

:- assert(worldGeneration("�����", 1)).
:- assert(renewable("�����", 1)).
:- assert(benefit("�����", 1)).
:- assert(highStrength("�����", 0)).
:- assert(craftable("�����", 1)).
:- assert(treasure("�����", 0)).

:- assert(worldGeneration("����", 0)).
:- assert(renewable("����", 1)).
:- assert(benefit("����", 1)).
:- assert(highStrength("����", 2)).
:- assert(craftable("����", 1)).
:- assert(treasure("����", 0)).

:- assert(worldGeneration("���� ����", 1)).
:- assert(renewable("���� ����", 1)).
:- assert(benefit("���� ����", 0)).
:- assert(highStrength("���� ����", 0)).
:- assert(craftable("���� ����", 1)).
:- assert(treasure("���� ����", 0)).

:- assert(worldGeneration("������", 1)).
:- assert(renewable("������", 0)).
:- assert(benefit("������", 0)).
:- assert(highStrength("������", 1)).
:- assert(craftable("������", 1)).
:- assert(treasure("������", 0)).

:- assert(worldGeneration("���", 0)).
:- assert(renewable("���", 0)).
:- assert(benefit("���", 1)).
:- assert(highStrength("���", 0)).
:- assert(craftable("���", 1)).
:- assert(treasure("���", 0)).

:- assert(worldGeneration("������� ����", 1)).
:- assert(renewable("������� ����", 1)).
:- assert(benefit("������� ����", 0)).
:- assert(highStrength("������� ����", 1)).
:- assert(craftable("������� ����", 1)).
:- assert(treasure("������� ����", 1)).

:- assert(worldGeneration("��������", 1)).
:- assert(renewable("��������", 0)).
:- assert(benefit("��������", 0)).
:- assert(highStrength("��������", 1)).
:- assert(craftable("��������", 0)).
:- assert(treasure("��������", 1)).

/* --- �������� --- */

:- assert(worldGeneration("�����", 0)).
:- assert(renewable("�����", 1)).
:- assert(benefit("�����", 0)).
:- assert(highStrength("�����", 0)).
:- assert(craftable("�����", 0)).
:- assert(treasure("�����", 1)).

:- assert(worldGeneration("������� ����", 0)).
:- assert(renewable("������� ����", 1)).
:- assert(benefit("������� ����", 1)).
:- assert(highStrength("������� ����", 0)).
:- assert(craftable("������� ����", 1)).
:- assert(treasure("������� ����", 0)).

:- assert(worldGeneration("�������� ������", 0)).
:- assert(renewable("�������� ������", 1)).
:- assert(benefit("�������� ������", 0)).
:- assert(highStrength("�������� ������", 0)).
:- assert(craftable("�������� ������", 1)).
:- assert(treasure("�������� ������", 1)).

:- assert(worldGeneration("������", 0)).
:- assert(renewable("������", 0)).
:- assert(benefit("������", 1)).
:- assert(highStrength("������", 0)).
:- assert(craftable("������", 1)).
:- assert(treasure("������", 1)).

:- assert(worldGeneration("������", 1)).
:- assert(renewable("������", 0)).
:- assert(benefit("������", 1)).
:- assert(highStrength("������", 1)).
:- assert(craftable("������", 0)).
:- assert(treasure("������", 0)).

:- assert(worldGeneration("������ ����", 0)).
:- assert(renewable("������ ����", 1)).
:- assert(benefit("������ ����", 1)).
:- assert(highStrength("������ ����", 0)).
:- assert(craftable("������ ����", 0)).
:- assert(treasure("������ ����", 1)).

:- assert(worldGeneration("������� �������", 0)).
:- assert(renewable("������� �������", 0)).
:- assert(benefit("������� �������", 0)).
:- assert(highStrength("������� �������", 0)).
:- assert(craftable("������� �������", 0)).
:- assert(treasure("������� �������", 0)).

:- assert(worldGeneration("������", 0)).
:- assert(renewable("������", 1)).
:- assert(benefit("������", 1)).
:- assert(highStrength("������", 0)).
:- assert(craftable("������", 0)).
:- assert(treasure("������", 1)).

:- assert(worldGeneration("�������� �����", 0)).
:- assert(renewable("�������� �����", 0)).
:- assert(benefit("�������� �����", 1)).
:- assert(highStrength("�������� �����", 1)).
:- assert(craftable("�������� �����", 1)).
:- assert(treasure("�������� �����", 1)).

:- assert(worldGeneration("���������� �����", 0)).
:- assert(renewable("���������� �����", 1)).
:- assert(benefit("���������� �����", 1)).
:- assert(highStrength("���������� �����", 0)).
:- assert(craftable("���������� �����", 1)).
:- assert(treasure("���������� �����", 0)).

:- assert(worldGeneration("�������� �����", 0)).
:- assert(renewable("�������� �����", 1)).
:- assert(benefit("�������� �����", 1)).
:- assert(highStrength("�������� �����", 2)).
:- assert(craftable("�������� �����", 1)).
:- assert(treasure("�������� �����", 1)).
