﻿open System

// Обработка числа
let rec Simple_ numberCurrent number numberSqrt =
    if (numberCurrent > numberSqrt) then
        true

    else
        if (number % numberCurrent = 0) then
            false

        else
            let nextNumber  = numberCurrent + 2
            Simple_ nextNumber number numberSqrt

// Интерфейс
let Simple (x : int) =
    let xSqrt = System.Math.Sqrt((float) x)
    let xSqrtInt = (int) xSqrt

    match x with
        |1 -> false
        |2 -> false
        |_ ->
            if (x % 2 = 0) then
                false
             else
                Simple_ 3 x xSqrtInt

// -----------------------------------------
// СЕДЬМАЯ
// -----------------------------------------

let rec NumberDigitsSum x =
    let rec NumberDigitsSum_ currentX sum =
        if (currentX = 0) then
            sum
        else
            let digit = currentX % 10
            let xNext = currentX / 10
            let sumNext = sum + digit
            NumberDigitsSum_ xNext sumNext

    NumberDigitsSum_ x 0

// -----------------------------------------
// ВОСЬМАЯ
// -----------------------------------------

let rec Gcd (x: int) (y : int) =
    if not(x = 0) && not(y = 0) then
        if (x > y) then
            let xNext = x % y
            Gcd xNext y

        else
           let yNext = y % x
           Gcd x yNext

    else
        x + y

[<EntryPoint>]
let main argv =
    System.Console.WriteLine("Hello World from F#!")
    System.Console.WriteLine(Gcd 30 18)
    0 // return an integer exit code