/*
������ ������� �����.
���� �������� ��������� ������, �������������� ����� ������ ���������:
[
	[������������1, [�������1, �������2 ... ]],
	[������������2, [�������1, �������2 ... ]]
	...
]

�������� ����:
   5
  /.\
 /.  \
3 - - 4
|.\./.|
|./.\.|
1 - - 2

��� ���������� ������ ������������ assert � retract (������� /��������� ����). ��� �������
����� ������ � �������, ������ ������������� ������ � ��������� � �� �������� ����
������: [ [1, [2, 3 ... ]], [1, [1, 3 ...]], [������������, [�������]] ... ]. ��� ��� �������� ����
���������� ��������� ���������, � ��� ����� ����� ����������� ������� � ���� ������� ����������
����������.
*/

% �� ������

inList([El|_],El).
inList([_|T],El):-inList(T,El).

read_str(A):-get0(X),r_str(X,A,[]).
r_str(10,A,A):-!.
r_str(X,A,B):-append(B,[X],B1),get0(X1),r_str(X1,A,B1).

write_str([]):-!.
write_str([H|Tail]):-put(H),write_str(Tail).

write_str_list([H|T], Divider) :-
	write_str(H), put(Divider),
	write_str_list(T, Divider).

write_str_list([], _).

% �������� �� ������� Vertex ������� �� ����� � ������?
hasEdgeForAll([H|T], Vertex) :-
	(
		hasEdge(H, Vertex);
		hasEdge(Vertex, H)
	),
	hasEdgeForAll(T, Vertex), !.

hasEdgeForAll([], Vertex).

% ���������� ��� �������, ������� �� ����� ��������� �� ������.
hasEdgeForAllFind([H|T], Vertex) :-
	(
		hasEdge(H, Vertex);
		hasEdge(Vertex, H)
	),
	hasEdgeForAllFind(T, Vertex).

hasEdgeForAllFind([], Vertex).

% �������� ��� ������� vertex ��� ��������������
deleteAll([H|T]) :-
	vertex(H),
	retract(vertex(H)),
	deleteAll(T), !.

deleteAll([H|T]) :-
	not(vertex(H)),
	deleteAll(T), !.

deleteAll([]) :- !.

% �������������� ������ ([a, b, c, d] -> [d, c, b, a]).
reverse(A, Z) :- reverse(A,Z,[]).
reverse([],Z,Z).
reverse([H|T],Z,Acc) :- reverse(T,Z,[H|Acc]).

% ���� ������ � ����������
getVertexes :-
	read_str(VertexesStr),
	getVertexes(VertexesStr, []).

getVertexes([H|T], CurrentVertex) :-
	H = 32, % ���������� ������
	reverse(CurrentVertex, CurrentVertexRev),
	assert(vertex(CurrentVertexRev)),
	getVertexes(T, []), !.

getVertexes([H|T], CurrentVertex) :-
	not(H = 32),
	getVertexes(T, [H|CurrentVertex]), !.

getVertexes([], CurrentVertex) :-
	reverse(CurrentVertex, CurrentVertexRev),
	assert(vertex(CurrentVertexRev)), !.

getVertexes([], []) :- fail, !.

% ���� ����� � ����������
getEdges :-
	read_str(EdgeStr),
	not(len(EdgeStr) = 0),
	processEdge(EdgeStr, [], []),
	getEdges.

processEdge([H|T], CurrentVertex, Vertexes) :-
	H = 32, % ���������� ������
	reverse(CurrentVertex, CurrentVertexRev),
	processEdge(T, [], [CurrentVertexRev|Vertexes]).

processEdge([H|T], CurrentVertex, Vertexes) :-
	not(H = 32),
	processEdge(T, [H|CurrentVertex], Vertexes).

processEdge([], FirstVertex, [SecondVertex|_]) :-
	assert(hasEdge(FirstVertex, SecondVertex)),
	assert(hasEdge(SecondVertex, FirstVertex)), !.

/* �������� �����-������� */

getCliques(FromPython) :-
	dynamic(hasEdge/2),
	dynamic(vertex/1),
	dynamic(pythonOutput/1),

	write("������� ������� �����. ����� ������������� �����. ����������� - ���� ������: "), nl,
	getVertexes,

	write("������� ����� �����. � ���� ������ ������� ���� �����, �������� �������� ������� (��������, 2-5). ����� ����������������. ������ ������ - ����� �����: "), nl,
	not(getEdges),

	nl, nl, write("������� �������: "), nl,
	forall(
		(
			vertex(X)
		),
		(
			write_str(X), write(", ")
		)
	),
	nl,

	write("������� �����: "), nl,
	forall(
		(
			hasEdge(X, Y)
		),
		(
			write_str(X), write("  -  "), write_str(Y), nl
		)
	),
	nl,

	write("������� �����: "), nl,
	forall(
		(
			hasEdge(StartFirst, StartSecond),
			vertex(StartSecond)
		),
		(
			getCliques_([StartFirst, StartSecond])
		)
	),

	abolish(vertex/1),
	abolish(hasEdge/2),
	abolish(pythonOutput/1).

getCliques_(Stack) :-
	hasEdgeForAll(Stack, Third),
	not(inList(Stack, Third)),
	getCliques_([Third|Stack]).

getCliques_(Stack) :-
	not(hasEdgeForAll(Stack, Third)),
	write_str_list(Stack, 32), nl,
	assert(pythonOutput(Stack)),
	deleteAll(Stack).

getCliquesAutomatic :-
	dynamic(hasEdge/2),
	dynamic(vertex/1),
	dynamic(pythonOutput/1),

	assert(hasEdge(1, 2)),
	assert(hasEdge(1, 3)),
	assert(hasEdge(1, 4)),
	assert(hasEdge(2, 3)),
	assert(hasEdge(2, 4)),
	assert(hasEdge(3, 4)),
	assert(hasEdge(3, 5)),
	assert(hasEdge(4, 5)),

	assert(vertex(1)),
	assert(vertex(2)),
	assert(vertex(3)),
	assert(vertex(4)),
	assert(vertex(5)),

	forall(
		(
			hasEdge(StartFirst, StartSecond),
			vertex(StartSecond)
		),
		(
			getCliquesAutomatic_([StartFirst, StartSecond])
		)
	),

	abolish(vertex/1),
	abolish(hasEdge/2),
	abolish(pythonOutput/1).

getCliquesAutomatic_(Stack) :-
	hasEdgeForAll(Stack, Third),
	not(inList(Stack, Third)),
	getCliquesAutomatic_([Third|Stack]).

getCliquesAutomatic_(Stack) :-
	not(hasEdgeForAll(Stack, Third)),
	write(Stack), nl,
	assert(pythonOutput(Stack)),
	deleteAll(Stack).
