open System

let INF = 32000

// Максимум двух чисел
let max x y =
    if (x > y) then
        x
    else
        y

// Возведение числа x в степень y. На всякий случай вместо pown
let pow x y =
    let rec pow_ acc x y =
        if (y = 1) then
            acc
        else
            if (y = 0) then
                0
            else
                pow_ (acc*x) x (y-1)

    pow_ x x y

// Проверяет все степени чисел от 2 до n. Показатели меньше, чем m. Из этих степеней выбирает наименьшую, большую x.
// Если не найдена, возвращает 32 000. Local ищет для одного значения n.
let minimalDegree x n m =
    let rec getLocalMinimalDegree acc curM x n m =
        if (acc > x) then
            acc
        else
            if (curM < m) then
                getLocalMinimalDegree (acc*n) (curM + 1) x n m
            else
                INF

    let rec minimalDegree_ acc x n m =
        if (n > 1) then
            let thisLocal = getLocalMinimalDegree (n*n) 2 x n m
            if (thisLocal < acc) then
                minimalDegree_ thisLocal x (n-1) m
            else
                minimalDegree_ acc x (n-1) m
        else
            acc

    minimalDegree_ INF x n m

// Сама ИЗ
let iz9 n m =
    let rec iz9_ (x: int) n m =
        Console.WriteLine("{0} ", x) |> ignore
        let nextX = minimalDegree x n m
        if (nextX = INF) then
            0
        else
            iz9_ nextX n m

    iz9_ 2 n m

[<EntryPoint>]
let main argv =
    iz9 5 5 |> ignore
    0 // return an integer exit code
