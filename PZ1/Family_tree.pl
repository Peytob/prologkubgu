/* DATA */

man('Petya').
man('Gena').
man('Arkasha').
man('Vasgen').

woman('Vika').
woman('Valya').

wife('Petya', 'Vika').
husband('Vika', 'Petya').

child('Petya', 'Gena').
child('Petya', 'Arkasha').
child('Petya', 'Valya').
child('Vika', 'Gena').
child('Vika', 'Arkasha').
child('Vika', 'Valya').

wife('Vasgen', 'Valya').
husband('Valya', 'Vasgen').

child('Vasgen', 'Kirill').
child('Valya', 'Kirill').

/* PREDICATES */

mother(Mother, Child) :-
    woman(Mother),
    child(Mother, Child).

father(Father, Child) :-
    man(Father),
    child(Father, Child).

grandma(GrandmaName, ChildName) :-
    mother(GrandmaName, ParentName),
    child(ParentName, ChildName).

% $Brother$ is brother $To$.
brother(Brother, To) :-
    child(X, Brother),
    child(X, To),
    man(Brother).

% $Sister$ is sister $To$.
sister(Sister, To) :-
    child(X, Sister),
    child(X, To),
    woman(Sister).

% ����������
uncle(Uncle, To) :-
    child(Old, Uncle),
    mother(Old, Parent),
    not(Uncle=Parent),
    child(Parent, To).

aunt(Aunt, To) :-
    child(Old, Aunt),
    father(Old, Parent),
    not(Aunt=Parent),
    child(Parent, To).

% Write list of sisters %Name%
getSisters(Name) :-
    mother(Mother, Name),
    child(Mother, Sister),
    woman(Sister),
    not(Sister=Name),
    write(Sister), nl.

getSisters(Name) :-
    father(Mother, Name),
    child(Mother, Sister),
    woman(Sister),
    not(Sister=Name),
    write(Sister), nl.

getBrothers(Name) :-
    mother(Mother, Name),
    child(Mother, Brother),
    man(Brother),
    not(Brother=Name),
    write(Brother), nl.

getBrothers(Name) :-
    father(Mother, Name),
    child(Mother, Brother),
    man(Brother),
    not(Brother=Name),
    write(Brother), nl.











