summOfList :-
    read(N),
    readList(A, N),
    summList(A, Sum),
    write(Sum).

readList(A, N) :-
    readList([], A, 0, N).

readList(A, A, N, N) :- !.

readList(List, A, I, N) :-
    I1 is I + 1,
    read(X),
    append(List, [X], List1).

summList(A, Sum) :- summList(A, 0, Sum).

summList([], Sum, Sum) :- !.

sumList([H|T], S, Sum) :-
    S1 is S + H,
    sumList(T, S1, Sum).

getNode(List, Number) :- getNode(List, Number, 0).

getNode([H|T], Number, CurrentNumber) :-
    (CurrentNumber = Number),
    write(H), !.

getNode([H|T], Number, CurrentNumber) :-
    Next is CurrentNumber + 1,
    getNode(T, Number, Next).
