:-consult("IZ-7-RussianBase").

quest1(X) :-
	write('����� �� ����� ������ ����/������� � ����������� ��������� ����? ��������� �� ���������, ������ � ����'), nl,
	write('1. ��'), nl,
	write('0. ���'), nl,
	write('3. �� ����'), nl,
	read(X).

quest2(X) :-
	write('�������� �� ������ ����/������� ��������������?'), nl,
	write('1. ��'), nl,
	write('0. ���'), nl,
	write('3. �� ����'), nl,
	read(X).

quest3(X) :-
	write('����� �� ������ ����/������� ������������ ������? ������������ ������� ���������: ���������, ������������� � ����������, ����������� ������.'), nl,
	write('1. ��'), nl,
	write('0. ���'), nl,
	write('3. �� ����'), nl,
	read(X).

quest4(X) :-
	write('����� �� ������ ����/������� ������� ��������� (���� �������) / ������������������ (���� ����)?'), nl,
	write('1. ��'), nl,
	write('0. ���'), nl,
	write('2. ��������'), nl,
	write('3. �� ����'), nl,
	read(X).

quest5(X) :-
	write('����� �� ����� ������� ������ ����/�������?'), nl,
	write('1. ��'), nl,
	write('0. ���'), nl,
	write('3. �� ����'), nl,
	read(X).

quest6(X) :-
	write('����������� �� ������ ������� � ���� ���������? ��������� ������ ������� � �����-���� �������, ����� ����������.'), nl,
	write('1. ��'), nl,
	write('0. ���'), nl,
	write('3. �� ����'), nl,
	read(X).

updateAnswer(Q, _) :-
	Q = 3.

updateAnswer(Q, R) :-
	not(Q = 3),
	Q = R.

findAnswers(Q1, Q2, Q3, Q4, Q5, Q6, BlockOrItem) :-
	worldGeneration(BlockOrItem, Q1),
	renewable(BlockOrItem, Q2),
	benefit(BlockOrItem, Q3),
	highStrength(BlockOrItem, Q4),
	craftable(BlockOrItem, Q5),
	treasure(BlockOrItem, Q6).

akinatorQuestionnaire :-
	quest1(Q1),
	updateAnswer(Q1, Answer1),
	quest2(Q2),
	updateAnswer(Q2, Answer2),
	quest3(Q3),
	updateAnswer(Q3, Answer3),
	quest4(Q4),
	updateAnswer(Q4, Answer4),
	quest5(Q5),
	updateAnswer(Q5, Answer5),
	quest6(Q6),
	updateAnswer(Q6, Answer6),

	forall(
		(
			findAnswers(Answer1, Answer2, Answer3, Answer4, Answer5, Answer6, Res)
		),
		(
			assert(answerResult(Res, Answer1, Answer2, Answer3, Answer4, Answer5, Answer6))
		)
	),
	% ������������ ��� ���������� � �������� ����������, ���� ����� �� ��� ������.
	assert(answerResult("", Answer1, Answer2, Answer3, Answer4, Answer5, Answer6)).

akinator :-
	dynamic(answerResult/7),
	akinatorQuestionnaire,
	magic,
	abolish(answerResult/7).

magic :-
	(
		answerResult(X, _, _, _, _, _, _),
		not(X = ""),
		write("����. �������� ����� ���������� "), write(X), write("! ������?"), nl,
		write("1 - ��! ��� ��� ����������!"), nl,
		write("2 - ���! :�"), nl,
		read(A),
		A = 1
	);
	(
		answerResult("", Answer1, Answer2, Answer3, Answer4, Answer5, Answer6),
		not(Answer1 = 3),
		not(Answer2 = 3),
		not(Answer3 = 3),
		not(Answer4 = 3),
		not(Answer5 = 3),
		not(Answer6 = 3),

		write("� �������! :( ��� �� ��� �����? ������� �����: "),
		read(ReadedItemWorkPlease), nl,
		write("�������� ����� �������: "), write(ReadedItemWorkPlease), nl,

		assert(worldGeneration(ReadedItemWorkPlease, Answer1)),
		assert(renewable(ReadedItemWorkPlease, Answer2)),
		assert(benefit(ReadedItemWorkPlease, Answer3)),
		assert(highStrength(ReadedItemWorkPlease, Answer4)),
		assert(craftable(ReadedItemWorkPlease, Answer5)),
		assert(treasure(ReadedItemWorkPlease, Answer6)),

		save('IZ-7-RussianBase-updated.pl')
	);
	(
		answerResult("", Answer1, Answer2, Answer3, Answer4, Answer5, Answer6),
		(
			Answer1 = 3;
			Answer2 = 3;
			Answer3 = 3;
			Answer4 = 3;
			Answer5 = 3;
			Answer6 = 3
		),
		write("� �������! � ���������, ��� ������� �������� ��������, ������� � ������� ������� (������� �����(�) 3). ������� ���������� ����������"), nl
	).

save(Filename) :-
	tell(Filename),
	write("/*"), nl,
	write("����� � �������� �� ������������ ���� Minecraft. ������ ���� ��� ���������!"), nl,
	write("	�����������:"), nl,
	write("	1. ����� �� ����� ������ ����/������� � ����������� ��������� ����? ��������� �� ���������"), nl,
	write("	2. �������� �� ������ ����/������� ��������������?"), nl,
	write("	3. ����� �� ������ ����/������� ������������ ������? ���������, �������, ��������� � ��� �����."), nl,
	write("	4. ����� �� ������ ����/������� ������� ��������� (���� �������)/������������������ (���� ����)."), nl,
	write("	5. ����� �� ����� ������� ������ ����/�������?"), nl,
	write("	6. ����������� �� ������ ������� � ���� ���������?"), nl,
	write("	��������: ����������� �������� ��������� � ������ ����������� ��� ���� �������!"), nl,
	write("	������ �������� �������� ������������ ������������� ����������� �� ��������������� ���������� ��� �������� ����� �� 12 ��� �"), nl,
	write("	��������� ������ ������������ ���� \"Minecraft\"."), nl,
	write("*/"), nl,
	nl,
	write("/* ������"), nl,
	write("0 - ���"), nl,
	write("1 - ��"), nl,
	write("2 - ��������"), nl,
	write("3 - �� ����"), nl,
	write("*/"), nl,
	nl,
	write("/* ��������� */"), nl,
	write(":- dynamic worldGeneration/2. % ����� �� ����� ������ ����/������� � ����������� ��������� ����? ��������� �� ���������"), nl,
	write(":- dynamic renewable/2. % �������� �� ������ ����/������� ��������������?"), nl,
	write(":- dynamic benefit/2. % ����� �� ������ ����/������� ������������ ������?"), nl,
	write(":- dynamic highStrength/2. % ����� �� ������ ����/������� ������� ��������� (���� �������)/������������������ (���� ����)."), nl,
	write(":- dynamic craftable/2. % ����� �� ����� ������� ������ ����/�������?"), nl,
	write(":- dynamic treasure/2. % ����������� �� ������ ������� � ���� ���������?"), nl,
	nl,
	write("/* ���� */"), nl,
	nl,
	forall(
		(
			worldGeneration(BlockOrItem, _)
		),
		(
			worldGeneration(BlockOrItem, Q1),
			renewable(BlockOrItem, Q2),
			benefit(BlockOrItem, Q3),
			highStrength(BlockOrItem, Q4),
			craftable(BlockOrItem, Q5),
			treasure(BlockOrItem, Q6),

			write("worldGeneration(\""), write(BlockOrItem), write("\", "), write(Q1), write("),"), nl,
			write("renewable(\""),       write(BlockOrItem), write("\", "), write(Q2), write("),"), nl,
			write("benefit(\""),         write(BlockOrItem), write("\", "), write(Q3), write("),"), nl,
			write("highStrength(\""),    write(BlockOrItem), write("\", "), write(Q4), write("),"), nl,
			write("craftable(\""),       write(BlockOrItem), write("\", "), write(Q5), write("),"), nl,
			write("treasure(\""),        write(BlockOrItem), write("\", "), write(Q6), write("),"), nl,
			nl
		)
	),
	told.
