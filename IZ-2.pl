% Считает вхождения элемента в список
countItem(List, Target, Counter) :- countItem(List, Target, Counter, 0).

countItem([H|T], Target, Counter, X) :-
	H = Target,
	X1 is X + 1,
	countItem(T, Target, Counter, X1).

countItem([H|T], Target, Counter, X) :-
	not(H = Target),
	countItem(T, Target, Counter, X).

countItem([], _, Counter, X) :-
	Counter is X.

getMoreThree([InH|InT], OutList) :-
	getMoreThree([InH|InT], [], OutList).

% Проверяет вхождение элемента. Просто переписанная версия стандартного member/2.
% Да, подумал об этом перед сдачей индивидуалки.
myMember(_, []) :- fail.

myMember(X, [H|T]) :-
	H = X, !.

myMember(X, [H|T]) :-
	not(H = X),
	myMember(T, X).

% Элемент встречается более 3 раз и еще не учтен
getMoreThree([InH|InT], TempList, OutList) :-
	countItem(InT, InH, X),  % Мы не учитываем еще 1 ед в голове!
	X >= 2,                  % Поэтому более двух.
	not(myMember(InH, TempList)),
	append(TempList, [InH], AppendList),
	getMoreThree(InT, AppendList, OutList), !.

% Элемент встречается более 3 раз и уже учтен
getMoreThree([InH|InT], TempList, OutList) :-
	countItem(InT, InH, X),  % Мы не учитываем еще 1 ед в голове!
	X >= 2,                  % Поэтому более двух.
	myMember(InH, TempList),
	getMoreThree(InT, TempList, OutList), !.

% Элемент встречается менее 3 раз. Ему нельзя проходить дальше...
getMoreThree([InH|InT], TempList, OutList) :-
	countItem(InT, InH, X),  % Мы не учитываем еще 1 ед в голове!
	not(X >= 2),             % Поэтому более двух.
	getMoreThree(InT, TempList, OutList), !.

getMoreThree([], TempList, OutList) :-
	OutList = TempList, !.
